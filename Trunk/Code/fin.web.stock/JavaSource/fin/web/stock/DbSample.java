package fin.web.stock;

import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import smart.base.datastore.utils.DbUtils;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

public class DbSample {
  public void testConnection() throws SQLException, UnsupportedEncodingException, ClassNotFoundException {
    Class.forName("com.mysql.jdbc.Driver");
    String jdbcUrl = "jdbc:mysql://localhost:3306/mysql";
    Map<String, String> params = new HashMap<String, String>();
    params.put("user", "stock");
    params.put("password", "stock@123");
    params.put("useSSL", "false");
    Connection conn = DbUtils.getConnection(jdbcUrl, params);

    String sql = "SELECT 1 as num FROM dual";
    try {
      Map<String, Object> map = getQueryResult(sql, conn);
      System.out.println(map.get("num"));
    } finally {
      DbUtils.closeConnection(conn);
    }

  }

  /**
   * 获取sql查询结果
   * 
   * @return Map<String,Object>
   */
  private Map<String, Object> getQueryResult(String sql, Connection conn) throws SQLException {
    List<Map<String, Object>> list = getQueryResultList(sql, conn);
    if (list.size() > 0) {
      return list.get(0);
    }
    return new HashMap<String, Object>();
  }

  /**
   * 获取sql查询结果
   * 
   * @return List<Map<String,Object>>
   */
  private List<Map<String, Object>> getQueryResultList(String sql, Connection conn) throws SQLException {
    List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
    Statement stmt = null;
    ResultSet rs = null;
    try {
      stmt = conn.createStatement();
      rs = stmt.executeQuery(sql);
      ResultSetMetaData metaData = rs.getMetaData();
      int columnCount = metaData.getColumnCount();
      while (rs.next()) {
        Map<String, Object> map = new HashMap<String, Object>();
        for (int i = 0; i < columnCount; i++) {
          int columnIndex = i + 1;
          String columnLabel = metaData.getColumnLabel(columnIndex);
          Object columnValue = rs.getObject(columnIndex);
          map.put(columnLabel, columnValue);
        }
        list.add(map);
      }
    } finally {
      DbUtils.closeResultSet(rs);
      DbUtils.closeStatement(stmt);
    }
    return list;
  }

  public void testHikariCP() throws SQLException, ClassNotFoundException {
    Class.forName("com.mysql.jdbc.Driver");
    
    HikariConfig config = new HikariConfig();
    config.setJdbcUrl("jdbc:mysql://localhost:3306/mysql");
    config.setUsername("stock");
    config.setPassword("stock@123");
    config.addDataSourceProperty("useSSL", "false");

    HikariDataSource ds = new HikariDataSource(config);
    try {
      Connection conn = ds.getConnection();
      Statement stmt = null;
      ResultSet rs = null;

      try {
        stmt = conn.createStatement();
        rs = stmt.executeQuery("SELECT 1 as num FROM dual");
        while (rs.next()) {
          String num = rs.getString("num");
          System.out.println(num);
        }
      } finally {
        DbUtils.closeResultSet(rs);
        DbUtils.closeStatement(stmt);
        DbUtils.closeConnection(conn);
      }
    } finally {
      ds.close();
    }
  }

  public static void main(String[] args) throws Exception {
    DbSample test = new DbSample();
    test.testConnection();
    test.testHikariCP();
  }

}
